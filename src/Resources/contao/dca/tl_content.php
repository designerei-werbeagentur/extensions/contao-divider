<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['palettes']['divider'] =
    '{type_legend},type;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID;'
    . '{invisible_legend:hide},invisible,start,stop'
;

PaletteManipulator::create()
  ->addField('blockDefault', 'expert_legend', PaletteManipulator::POSITION_APPEND)
  ->applyToPalette('divider', 'tl_content')
;
